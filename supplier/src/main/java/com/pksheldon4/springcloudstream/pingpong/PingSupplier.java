package com.pksheldon4.springcloudstream.pingpong;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Component
@Slf4j
public class PingSupplier implements Supplier<String> {

    private static final String PING = "Ping";
    private static final String PONG = "Pong";

    private int pingNumber = 1;

    @Override
    public String get() {
        String response = PING + " " + pingNumber++;
        log.info("Supplying {}", response);
        return response;
    }
}
