package com.pksheldon4.springcloudstream.pingpong;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class PingPongSupplierApplication {

    public static void main(String[] args) {
        SpringApplication.run(PingPongSupplierApplication.class, args);
    }

}
