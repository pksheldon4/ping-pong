package com.pksheldon4.springcloudstream.pingpong;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

@Slf4j
@Component
public class PingConsumer implements Consumer<String> {

    @Override
    public void accept(String message) {
        log.info("Consuming {}", message);
    }
}
