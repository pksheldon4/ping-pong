package com.pksheldon4.springcloudstream.pingpong;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.context.annotation.Bean;

import java.util.function.Consumer;

@SpringBootApplication
@EnableBinding(Processor.class)
@Slf4j
public class PingPongConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PingPongConsumerApplication.class, args);
        log.info("The PingPong Application has started...");
    }
}
